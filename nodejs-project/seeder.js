import mongoose from 'mongoose'
import dotenv from 'dotenv'
import colors from 'colors'
import user from './data/user.js'
import User from './models/userModel.js'
import connectDB from './config/db.js'

dotenv.config()

connectDB()

const importData = async () => {
    try {

        await User.deleteMany()

        const createdUser = await User.insertMany(user)

        const adminUser = createdUser[0]._id

        console.log('Data imported !'.green.inverse)
        process.exit()

    } catch (error) {
        
        console.error(`${error}`.red.inverse)
        process.exit(1)
    }
}

const destroyData = async () => {
    try {
       
        console.log('Data Destroyed !'.red.inverse)
        process.exit()

    } catch (error) {
        console.error(`${error}`.red.inverse)
        process.exit(1)
    }
}   

if(process.argv[2] === '-d') {
    destroyData()
} else {
    importData()
}