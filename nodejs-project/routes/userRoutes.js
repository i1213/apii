import express from 'express'
const router = express.Router() 
import { authUser, getAllUsers, logout, registerUser } from '../controllers/userController.js'



router.route('/').post(registerUser).put(logout).get(getAllUsers)
router.post('/login', authUser)



export default router