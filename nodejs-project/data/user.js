import bcrypt from 'bcryptjs'

const user = [
    {
        FirstName: 'amine',
        LastName: 'fredj',
        Email: 'amin@example.com',
        Password: bcrypt.hashSync('123456', 10),
    },
    {
        FirstName: 'amine',
        LastName: 'fredj',
        Email: 'amin1@example.com',
        Password: bcrypt.hashSync('123456', 10),
    },
    {
        FirstName: 'amine',
        LastName: 'fredj',
        Email: 'amin2@example.com',
        Password: bcrypt.hashSync('123456', 10),
    }
]

export default user