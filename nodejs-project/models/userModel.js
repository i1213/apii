import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'


const userSchema = mongoose.Schema({
    FirstName: {
        type : String,
        require: true,
    } ,
    LastName: {
        type : String,
        require: true,
    },
    Email: {
        type : String,
        require: true,
        unique: true,
    },
    Password: {
            type : String,
            require: true,
    },
}, {
    timestamps: true
})

userSchema.methods.matchMdp = async function(enterdedPassword) {
    return await bcrypt.compare(enterdedPassword, this.Password)
}

userSchema.pre('save', async function (next) {
    if (!this.isModified('Password')) {
      next()
    }
    const salt = await bcrypt.genSalt(10)
    this.Password = await bcrypt.hash(this.Password, salt)
  })
const User = mongoose.model('User', userSchema) 

export default User