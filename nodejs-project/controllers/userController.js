import asyncHandler from 'express-async-handler'
import User from '../models/userModel.js'
import generateToken from '../utils/generateToken.js'





// @desc Auth User & get token 
// @route POST /users/login
// @acess Public
const authUser = asyncHandler(async(req,res) => {
   const { Email, Password } =  req.body

   const user = await User.findOne({ Email })

   if (user && (await user.matchMdp(Password))) {
        await user.save()
       res.json({
           _id: user._id,
           FirstName : user.FirstName,
           LastName: user.LastName,
           Email: user.Email,
           token: generateToken(user._id),
       })
      
   } else {
       res.status(401)
       throw new Error('Invalid Email or Password !')
   }
})

// @desc Logout
// @route Put /logout
// @acess Public
const logout = asyncHandler(async(req,res) => {
 
    const user = await User.findById(req.body._id)
 
    if (user) {
         await user.save() 
         res.json({
            _id: user._id,
            FirstName : user.FirstName,
            LastName: user.LastName,
            Email: user.Email,
        })
    } else {
        res.status(401)
        throw new Error('error !')
    }
 })

// @desc    Register a new user
// @route   POST /api/users
// @access  Public
const registerUser = asyncHandler(async (req, res) => {
    const { FirstName, LastName, Email, Password } = req.body
  
    const userExists = await User.findOne({ Email })
  
    if (userExists) {
      res.status(400)
      throw new Error('User already exists')
    }
  
    const user = await User.create({
      FirstName,
      LastName,
      Email,
      Password,
    })
  
    if (user) {
      res.status(201).json({
        _id: user._id,
        FirstName: user.FirstName,
        LastName: user.LastName,
        Email: user.Email,
        token: generateToken(user._id),
      })
    } else {
      res.status(400)
      throw new Error('Invalid user data')
    }
  })
 // @desc GET all users
// @route GET /admin/users
// @acess Private /admin
const getAllUsers = asyncHandler(async(req,res) => {
    const users = await User.find()
    res.json(users)
 })
export {authUser, logout, registerUser,getAllUsers}