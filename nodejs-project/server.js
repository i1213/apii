import  express from 'express'
import dotenv from 'dotenv'
import colors from 'colors'
import connectDB from './config/db.js'
import userRoutes from './routes/userRoutes.js'
import cors from 'cors'



dotenv.config()

connectDB()


const co = cors()

const app = express()

app.use(express.json())

app.use(co)


app.get('/',(req, res) => {
    res.send('API is running...')
})

app.use('/api/users', userRoutes) 


const PORT = process.env.PORT || 5000

app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode  on port ${PORT}`.yellow.bold))